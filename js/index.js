// start BT1
let printTo100=()=>{
    var contentHTML='';
    for (var i=0; i<=9; i++){
        for (var j=1; j<=10; j++){
            if (j==10 && i!=0){
                contentHTML=contentHTML+j*(i+1);
            } else if (j==10 && i==0){
                contentHTML=contentHTML+'10';
            } else{
                contentHTML=contentHTML+i+j+` `;
            };
        };
        contentHTML=contentHTML+`<br/>`
    };
    document.getElementById('printTo100').innerHTML=contentHTML;
};
// end BT1

// start BT2
let arrayBT2=[];
// render mảng ra màn hinh
let render=()=>{
    var contentHTML='';
    arrayBT2.forEach(item=>{
        contentHTML=contentHTML+' '+item;
    });
    document.getElementById('arrayBT2').innerHTML=contentHTML;
};
// validate dữ liệu đầu vào
let validate=(n)=>{
    var isValid=Number.isInteger(n);
    // ktra rỗng
    if (document.getElementById('n').value==''){
        alert('Vui lòng nhập số n');
        return false;
    };
    // Ktra số nguyên
    if (isValid==false){
        alert('Chỉ nhập số nguyên. Vui lòng nhập lại.');
        return false;
    }else{
        return true;
    };
};
// thêm n vào mảng
let add=()=>{
        var n=document.getElementById('n').value*1;
        var isValid=validate(n);
        if (isValid){
            arrayBT2.push(n);
            render();
        }; 
};
// trả về mảng rỗng
let reset=()=>{
    arrayBT2=[];
    render();
    document.getElementById('listPrimeNumber').innerHTML='';
};
// xác định số nguyên tố
let primeNumber=(n)=>{
    // quy ước: true=số nguyên tố, false=ko số nguyên tố
    if(n<2){
        return false;
    }else if (n==2 || n==3){
        return true;
    }else{ 
        for (var i=2; i<=Math.sqrt(n); i++){
            if (n%i==0){
                return false;
            }
        }
    };
    return true;
};
// tìm danh sách số nguyên tố
let listPrimeNumber=()=>{
    var contentHTML='';
    arrayBT2.forEach(item=>{
        if (primeNumber(item)){
            contentHTML=contentHTML+' '+item;
        };
    });
    if (contentHTML==''){
        document.getElementById('listPrimeNumber').innerHTML='Không có số nguyên tố nào';
    }else{
        document.getElementById('listPrimeNumber').innerHTML=`Số nguyên tố gồm: <br/>${contentHTML}`;
    };
};
// end BT2

// start BT3
let tinhS=()=>{
    var S=0;
    var n=document.getElementById('n2').value*1;
    if(n>=2 && Number.isInteger(n)){
        for(var i=2;i<=n;i++){
            S=S+i;
        };
        S=S+2*n;
        document.getElementById('tinhS').innerHTML=S;
    }else(
        alert('N cần là số nguyên và lớn hơn 2. Vui lòng nhập lại.')
    );
};
// end BT3

// start BT4

// end BT4

// // start BT5
// function primeNumber(n){
//     if (n==2 || n==3){
//         return true;
//     }else{ 
//         for (var i=2; i<=Math.sqrt(n); i++){
//             if (n%i==0){
//                 return false
//             }
//         }
//     }
//     return true;
// }

// function listPrimeNumber(){
//     var n3=document.getElementById('n3').value*1;
//     if (n3<0) {return alert('Vui lòng nhập lại số n >= 0')}

//     var result='';
//     for (var i=2; i<=n3; i++){
//         if (primeNumber(i)==true){
//             result=result+' '+i;
//         }
//     }
//     console.log(result);
//     document.getElementById('listPrimeNumber').innerHTML=`${result}`;
// }
// // end BT5

// start BT6
let timX=()=>{
    var S=0;
    var i=1;
    while((S+i)<=100){
            S=S+i;
            i++;        
    };
    if((S+i)>100){
        document.getElementById('timX').innerHTML=i-1;
    }else{
        document.getElementById('timX').innerHTML=i;
    };
}
// end BT6

// start BT7
let bangCuuChuong=()=>{
    contentHTML='';
    var n=document.getElementById('n7').value*1;
    for (var i=0; i<=10; i++){
        contentHTML=contentHTML+`${n} x ${i} = ${n*i} <br/>`;
    };
    document.getElementById('bangCuuChuong').innerHTML=contentHTML;
}
// end BT7